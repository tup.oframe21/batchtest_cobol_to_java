package com.tmax.openframe.file;

import ch.obermuhlner.math.big.BigDecimalMath;
import com.tmax.openframe.variable.group.*;
import com.tmax.proobject.dataobject.exception.EmptyResultException;
import com.tmax.proobject.dataobject.session.SessionContainer;
import java.math.*;
import java.sql.SQLException;
import java.util.*;
import lombok.*;

/** 
 * <p>This source code was generated by COBOL to JAVA migrator</p>
 */
public class COBTEST5_FD_OUT_F {
    private Long currentSeqId = null;
    private static final int DEFAULT_FETCH_SIZE = 100;
    private COBTEST5_COBTEST5_FD_OUT_F_Dof dof;
    private @Getter FileStatus fileStatus;
    private Iterator<COBTEST5_COBTEST5_FD_OUT_F_Do> forwardListIterator_COBTEST5_FD_OUT_F;

    public COBTEST5_FD_OUT_F() {
        dof = new COBTEST5_COBTEST5_FD_OUT_F_Dof();
        forwardListIterator_COBTEST5_FD_OUT_F = null;
    }

    public void start(Long key) {
        currentSeqId = key;
        dof.setSeq_Id(currentSeqId);
        dof.setFullQuery(COBTEST5_COBTEST5_FD_OUT_F_Dof.FULLQUERY.READ_GE);
        forwardListIterator_COBTEST5_FD_OUT_F = dof.getForwardList().iterator();
        if (forwardListIterator_COBTEST5_FD_OUT_F.hasNext()) {
            fileStatus = FileStatus.INVALID_KEY_READ;
            return;
        }
        fileStatus = FileStatus.NORMAL;
    }

    public COBTEST5_EMTXDAC read() {
        if (fileStatus == FileStatus.AT_END_CONDITION_SEQUENCED) {
            fileStatus = FileStatus.ALREADY_AT_END_CONDITION;
            return null;
        }
        COBTEST5_COBTEST5_FD_OUT_F_Do tmp = null;
        if (forwardListIterator_COBTEST5_FD_OUT_F != null) {
            if (forwardListIterator_COBTEST5_FD_OUT_F.hasNext()) {
                tmp = forwardListIterator_COBTEST5_FD_OUT_F.next();
                currentSeqId = tmp.getSeq_Id();
            } else {
                fileStatus = FileStatus.AT_END_CONDITION_SEQUENCED;
                return null;
            }
        } else {
            dof.setFullQuery(COBTEST5_COBTEST5_FD_OUT_F_Dof.FULLQUERY.READ_GE);
            dof.setSeq_Id(getNextSeqId());
            forwardListIterator_COBTEST5_FD_OUT_F = dof.getForwardList()
                    .iterator();
            if (forwardListIterator_COBTEST5_FD_OUT_F.hasNext()) {
                tmp = forwardListIterator_COBTEST5_FD_OUT_F.next();
                currentSeqId = tmp.getSeq_Id();
            } else {
                fileStatus = FileStatus.AT_END_CONDITION_SEQUENCED;
                return null;
            }
        }
        fileStatus = FileStatus.NORMAL;
        return COBTEST5_COBTEST5_FD_OUT_F_DoToCOBTEST5_EMTXDACMapper.INSTANCE
                .toCOBTEST5_EMTXDAC(tmp);
    }

    public COBTEST5_EMTXDAC readNext() {
        return read();
    }

    public COBTEST5_EMTXDAC readKey(Long key) {
        currentSeqId = key;
        dof.setSeq_Id(currentSeqId);
        dof.setFullQuery(COBTEST5_COBTEST5_FD_OUT_F_Dof.FULLQUERY.READ_EQ);
        forwardListIterator_COBTEST5_FD_OUT_F = dof.getForwardList().iterator();
        COBTEST5_EMTXDAC record = read();
        if (record == null) {
            fileStatus = FileStatus.INVALID_KEY_READ;
            return null;
        }
        return record;
    }

    public void rewrite(COBTEST5_EMTXDAC data, Long seq) {
        COBTEST5_COBTEST5_FD_OUT_F_Do tmpObject = COBTEST5_COBTEST5_FD_OUT_F_DoToCOBTEST5_EMTXDACMapper.INSTANCE
                .toCOBTEST5_COBTEST5_FD_OUT_F_Do(data);
        dof.setFullQuery(COBTEST5_COBTEST5_FD_OUT_F_Dof.FULLQUERY.REWRITE);
        tmpObject.setSeq_Id(seq);
        try {
            dof.update(tmpObject);
            fileStatus = FileStatus.NORMAL;
        } catch (EmptyResultException ex) {
            fileStatus = FileStatus.INVALID_KEY_CREATE;
        }
    }

    public void rewrite(COBTEST5_EMTXDAC data) {
        rewrite(data, currentSeqId);
    }

    public void write(COBTEST5_EMTXDAC data) {
        dof.setFullQuery(COBTEST5_COBTEST5_FD_OUT_F_Dof.FULLQUERY.WRITE);
        dof.add(COBTEST5_COBTEST5_FD_OUT_F_DoToCOBTEST5_EMTXDACMapper.INSTANCE
                .toCOBTEST5_COBTEST5_FD_OUT_F_Do(data));
        fileStatus = FileStatus.NORMAL;
    }

    public int delete() {
        return _delete(currentSeqId);
    }

    public int delete(Long key) {
        int cnt = _delete(key);
        if (cnt == 0) {
            fileStatus = FileStatus.INVALID_KEY_READ;
        } else {
            fileStatus = FileStatus.NORMAL;
        }
    }

    private int _delete(Long key) {
        if (dof == null || forwardListIterator_COBTEST5_FD_OUT_F == null) {
            return 0;
        }
        dof.setFullQuery(COBTEST5_COBTEST5_FD_OUT_F_Dof.FULLQUERY.DELETE);
        dof.setSeq_Id(currentSeqId);
        return dof.remove();
    }

    public void close() {
        SessionContainer.getInstance().commit();
        forwardListIterator_COBTEST5_FD_OUT_F = null;
        currentSeqId = null;
    }

    private Long getNextSeqId() {
        if (currentSeqId != null) {
            currentSeqId++;
        } else {
            currentSeqId = 1L;
        }
        return currentSeqId;
    }
}
