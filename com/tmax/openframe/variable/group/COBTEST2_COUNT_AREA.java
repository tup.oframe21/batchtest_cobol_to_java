package com.tmax.openframe.variable.group;

import ch.obermuhlner.math.big.BigDecimalMath;
import java.math.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.function.BiFunction;
import lombok.*;
import org.apache.commons.lang3.StringUtils;

/** 
 * <p>This source code was generated by COBOL to JAVA migrator</p>
 */
public @AllArgsConstructor @NoArgsConstructor @Builder(builderClassName = "builder") class COBTEST2_COUNT_AREA {
    public @Getter @Setter @Builder.Default long ITEM_CNT = (long) 0;
    public @Getter @Setter @Builder.Default long RVL_CNT = (long) 0;
    public @Getter @Setter @Builder.Default long ONLY_ITEM_CNT = (long) 0;
    public @Getter @Setter @Builder.Default long ONLY_RVL_CNT = (long) 0;
    public @Getter @Setter @Builder.Default long MATCH_CNT = (long) 0;
    public @Getter @Setter @Builder.Default long CONT_CNT = (long) 0;
    public @Getter @Setter @Builder.Default long WR_CNT = (long) 0;
    private final static BiFunction<Long, String, String> intFormatWithoutSign = (
            value, format) -> {
        DecimalFormat nf = new DecimalFormat();
        nf.setNegativePrefix("");
        nf.applyPattern(format);
        return nf.format(value);
    };

    public String getStringITEM_CNT() {
        return intFormatWithoutSign.apply(ITEM_CNT, "00000000000");
    }

    public String getStringRVL_CNT() {
        return intFormatWithoutSign.apply(RVL_CNT, "00000000000");
    }

    public String getStringONLY_ITEM_CNT() {
        return intFormatWithoutSign.apply(ONLY_ITEM_CNT, "00000000000");
    }

    public String getStringONLY_RVL_CNT() {
        return intFormatWithoutSign.apply(ONLY_RVL_CNT, "00000000000");
    }

    public String getStringMATCH_CNT() {
        return intFormatWithoutSign.apply(MATCH_CNT, "00000000000");
    }

    public String getStringCONT_CNT() {
        return intFormatWithoutSign.apply(CONT_CNT, "00000000000");
    }

    public String getStringWR_CNT() {
        return intFormatWithoutSign.apply(WR_CNT, "00000000000");
    }

    @Override
    public String toString() {
        return getStringITEM_CNT() + getStringRVL_CNT()
                + getStringONLY_ITEM_CNT() + getStringONLY_RVL_CNT()
                + getStringMATCH_CNT() + getStringCONT_CNT()
                + getStringWR_CNT();
    }
}
